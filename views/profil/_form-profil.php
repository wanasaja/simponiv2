<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-04 15:43:22
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-12 21:29:28
*/

use yii\helpers\Html;
// use yii\widgets\ActiveForm;
use kartik\form\ActiveForm;

?>

<div style="padding-top: 2%">
    <?php 
        $form = ActiveForm::begin([
            'id' => 'login-form-horizontal-1', 
            'type' => ActiveForm::TYPE_HORIZONTAL,
            'formConfig' => ['labelSpan' => 2, 'deviceSize' => ActiveForm::SIZE_SMALL],
            'action' => ['profil/ubah-profil?id='.$model->id]
        ]); 
    ?>
    
    <?= Html::activeHiddenInput($model, 'id', ['value' => $model->id]);  ?>

    <?= $form->field($model, 'nama_rs_puskesmas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kontak')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

  
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-9">
            <?= Html::submitButton('Simpan', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>